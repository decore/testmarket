$(document).ready(function(){

	$('.menu-toggle').click(function(){
		$('body').toggleClass('active');
	});
	$('.left-menu > li > a').click(function(e){
		if ($(this).parent().children().is('ul')){
			e.preventDefault();
			$(this).parent('li').addClass('active').siblings('.left-menu > li').removeClass('active');
		};
	});

	$('select').select2({
		allowClear: true,
		minimumResultsForSearch: -1
	});

	$('input').iCheck({
		labelHover: false,
		cursor: true,
		checkboxClass: 'icheckbox_minimal-blue',
		radioClass: 'iradio_minimal-blue',
		increaseArea: '20%'
	});

	$('.message-text').one("change keyup paste", function(){
		console.log('hi');
		$(this).parent().find('.btn').attr('disabled', false);
	});

	$('input, select').tooltip();

	$('.footable').footable();

});
